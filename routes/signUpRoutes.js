const signUpRoutes = (req,res,bcrypt,signUp,mongoose)=>{
    const {name,email,password} = req.body;
    const hash = bcrypt.hashSync(password);
    signUp.findOne({email}).exec().then(result1=>{
        if(result1){
            res.status(400).json('User already Registered');
        }else{
             const signup = new signUp({
                _id:mongoose.Types.ObjectId(),
                name:name,
                email:email,
                password:hash,
                reg_date:new Date()
            });
            signup.save()
            .then(result=>{
                console.log(result);
                res.status(200).json('user register successfully');
            })
            .catch(err=>{
                console.log(err);
                res.status(500).json(err);
            })
        }    
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json(err);
    })
}

module.exports = {signUpRoutes}