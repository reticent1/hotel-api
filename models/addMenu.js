const mongoose = require('mongoose');

const addmenu = mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
    name:String,
    description:String,
    price:Number,
    food_type:String
});

module.exports = mongoose.model('addMenu',addmenu);