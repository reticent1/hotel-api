const mongoose = require('mongoose');

const signUp = mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
    name:String,
    email:{type:String,unique:true},
    password:String,
    reg_date:Date
});

module.exports = mongoose.model('signUp',signUp);