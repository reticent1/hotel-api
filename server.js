//Packages
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const bcrypt = require('bcrypt-nodejs');

//connection
const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://restaurant:restaurant@restaurant-tp220.mongodb.net/test?retryWrites=true',{useNewUrlParser:true,useCreateIndex:true});

//models
const addMenu = require('./models/addMenu');
const signUp = require('./models/signupModel');

//routes
const addMenuRoutes = require('./routes/adMenuRoutes');
const getMenuRoutes = require('./routes/getMenuRoutes');
const signUpRoutes = require('./routes/signUpRoutes');
const signInRoutes = require('./routes/signInRoutes');

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());

//controllers
app.post('/addmenu',(req,res)=>{
    addMenuRoutes.addMenuRoutes(req,res,addMenu,mongoose);
});
app.get('/menu',(req,res)=>{
   getMenuRoutes.getMenuRoute(req,res,addMenu);
})
app.post('/signup',(req,res)=>{
  signUpRoutes.signUpRoutes(req,res,bcrypt,signUp,mongoose);
})
app.post('/signin',(req,res)=>{
  signInRoutes.signInRoutes(req,res,signUp,bcrypt);  
})

app.listen(3000,()=>{console.log("server running on port 3000")});